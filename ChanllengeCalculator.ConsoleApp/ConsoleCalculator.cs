﻿using ChallengeCalculator.Domain;
using ChallengeCalculator.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChanllengeCalculator.ConsoleApp
{
    class ConsoleCalculator
    {
        public void Start()
        {
            Console.WriteLine("Welcome to challenge calculator!");
            var option = ConsoleKey.C;
            while (option == ConsoleKey.C)
            {
                Console.WriteLine();
                ShowSelectOperationMessage();
                var operatorSelected = GetSelectedOperation();
                Console.WriteLine();
                Console.WriteLine($"Type the numbers you want to use in the {operatorSelected} operation.");
                var input = Console.ReadLine();

                try
                {
                    var formattedStringInput = new FormattedStringInput();
                    var numbersEnumerable = formattedStringInput.GetNumbersEnumerableFromInput(input);
                    var operands = Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable);
                    var formulaBuilder = new FormulaBuilder();
                    var calculator = new Calculator(formulaBuilder);

                    var result = calculator.ExecuteOperation(operands, operatorSelected);

                    ShowResultMessage(input, result, operatorSelected);
                    ShowFormulaMessage(formulaBuilder.GetFormula());                    
                }

                catch (AllNegativeNumbersException e)
                {
                    ShowErrorNegativeNumbersMessage(e.NegativeNumbers);
                }
                catch (InvalidInputDelimiterPatternException)
                {
                    ShowErrorMessage($@"Invalid delimiter pattern in {input}. Use the format: 
//{{delimiter}}\n{{numbers}}
//[{{delimiter1}}][{{delimiter2}}]...\n{{numbers}}
");
                }
                catch (Exception e)
                {
                    ShowErrorMessage(e.Message);
                }

                Console.WriteLine("Press C key to continue or any other to finish.");
                option = Console.ReadKey().Key;
            }
        }

        private void ShowSelectOperationMessage()
        {
            Console.WriteLine("Select math operation you want to use. Addition will be selected by default.");
            Console.WriteLine("1- Addition");
            Console.WriteLine("2- Substraction");
            Console.WriteLine("3- Multiplication");
            Console.WriteLine("4- Division");
        }

        private Operator GetSelectedOperation()
        {
            var optionSelected = Console.ReadLine();
            switch (optionSelected)
            {
                case "2": return Operator.Substraction;
                case "3": return Operator.Multiplication;
                case "4": return Operator.Division;
                default: return Operator.Addition;
            }
        }

        private void ShowFormulaMessage(string formula)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"Formula used was {formula}");
            Console.ResetColor();
        }

        private void ShowResultMessage(string input, decimal result, Operator @operator)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"The result of the {@operator} operation with {input} as input is {result}");
            Console.ResetColor();
        }

        private void ShowErrorMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private void ShowErrorNegativeNumbersMessage(IEnumerable<decimal> negativeNumbers)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Negative numbers are not allowed. ");
            foreach (var n in negativeNumbers)
            {
                Console.Write(n + ", ");
            }

            Console.ResetColor();
        }
    }
}
