﻿using System;

namespace ChanllengeCalculator.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var consoleCalculator = new ConsoleCalculator();
            consoleCalculator.Start();
        }
    }
}
