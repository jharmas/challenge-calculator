﻿using ChallengeCalculator.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ChanllengeCalculator.UnitTests
{
    public class TestCalculator
    {
        [Fact]
        public void should_return_sum_of_all_operands()
        {
            var caluculator = new Calculator();
            var operands = Operands.CreateOperandsFromNumbersEnumerable(new[] { "1", "2", "3", "4"});

            Assert.StrictEqual(10, caluculator.ExecuteOperation(operands, Operator.Addition));
        }

        [Fact]
        public void should_return_substraction_of_all_operands()
        {
            var caluculator = new Calculator();
            var operands = Operands.CreateOperandsFromNumbersEnumerable(new[] { "10", "3", "4" });

            Assert.StrictEqual(3, caluculator.ExecuteOperation(operands, Operator.Substraction));
        }

        [Fact]
        public void should_return_multiplication_of_all_operands()
        {
            var caluculator = new Calculator();
            var operands = Operands.CreateOperandsFromNumbersEnumerable(new[] { "10", "3", "4" });

            Assert.StrictEqual(120, caluculator.ExecuteOperation(operands, Operator.Multiplication));
        }

        [Fact]
        public void should_return_division_of_all_operands()
        {
            var caluculator = new Calculator();
            var operands = Operands.CreateOperandsFromNumbersEnumerable(new[] { "100", "5", "4" });

            Assert.StrictEqual(5, caluculator.ExecuteOperation(operands, Operator.Division));
        }
    }
}
