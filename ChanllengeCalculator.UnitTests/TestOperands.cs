using ChallengeCalculator.Domain;
using ChallengeCalculator.Domain.Exceptions;
using System;
using System.Linq;
using Xunit;

namespace ChanllengeCalculator.UnitTests
{
    public class TestOperands
    {
        [Fact]
        public void CreateOperandsFromNumbersEnumerable_should_contain_all_operands_when_more_than_2_numbers()
        {
            var numbersEnumerable = new[] { "1", "2", "3" };
            var operands = Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable).GetAllOperands();

            //Check contains one operand equals 1
            Assert.Contains(operands, _ => _ == 1);

            //Check contains one operand equals 2
            Assert.Contains(operands, _ => _ == 2);

            //Check contains one operand equals 3
            Assert.Contains(operands, _ => _ == 3);
        }

        [Fact]
        public void CreateOperandsFromNumbersEnumerable_should_convert_to_0_invalid_or_missing_numbers()
        {
            //Missing scenario
            var numbersEnumerable = new string[] {};
            var operands = Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable).GetAllOperands();
            Assert.Single<decimal>(operands, _ => _ == 0);

            //Invalid scenario
            numbersEnumerable = new string[] { "5", "tytyt" };
            operands = Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable).GetAllOperands();
            
            //Check contains two operands
            Assert.True(operands.Count() == 2);
            
            //Check contains one operand equals 5
            Assert.Contains(operands, _ => _ == 5);
            
            //Check contains one operand equals 0 for "tytyt"
            Assert.Contains(operands, _ => _ == 0);
        }

        [Fact]
        public void CreateOperandsFromNumbersEnumerable_should_throw_AllNegativeNumbersException()
        {
            //Missing scenario
            var numbersEnumerable = new string[] { "-1", "5", "tyt", "-2"};

            Assert.Throws<AllNegativeNumbersException>(() => Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable));

            //Check the exception contains the right values
            try
            {
                Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable);
            }
            catch (AllNegativeNumbersException e)
            {
                Assert.NotNull(e.NegativeNumbers);

                Assert.Equal(2, e.NegativeNumbers.Count());

                //Check contains one operand equals 5
                Assert.Contains(e.NegativeNumbers, _ => _ == -1);

                //Check contains one operand equals 0 for "tytyt"
                Assert.Contains(e.NegativeNumbers, _ => _ == -2);
            }
        }

        [Fact]
        public void CreateOperandsFromNumbersEnumerable_should_ignore_numbers_greater_than_1000()
        {
            var numbersEnumerable = new string[] { "1001", "1000", "2000" };
            var operands = Operands.CreateOperandsFromNumbersEnumerable(numbersEnumerable).GetAllOperands();

            //Should contains only one element equals 1000
            Assert.Single<decimal>(operands, _ => _ == 1000);
        }
    }
}
