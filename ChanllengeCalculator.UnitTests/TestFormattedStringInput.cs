﻿using ChallengeCalculator.Domain;
using ChallengeCalculator.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ChanllengeCalculator.UnitTests
{
    public class TestFormattedStringInput
    {
        [Fact]
        public void GetNumbersEnumerableFromInput_should_split_input_using_comma_and_newline_character_delimiter()
        {
            var formattedStringInput = new FormattedStringInput();
            var input = "1\n2,3";

            //Check GetNumbersEnumerableFromInput return 3 elements enumarable 
            Assert.Equal(3, formattedStringInput.GetNumbersEnumerableFromInput(input).Count());
        }

        [Fact]
        public void GetNumbersEnumerableFromInput_should_split_input_using_single_custom_delimiter()
        {
            var formattedStringInput = new FormattedStringInput();
            var input = "//;\n2;5";

            //Check GetNumbersEnumerableFromInput return 2 elements enumarable 
            Assert.Equal(2, formattedStringInput.GetNumbersEnumerableFromInput(input).Count());
        }

        [Fact]
        public void GetNumbersEnumerableFromInput_should_split_input_using_any_length_custom_delimiter()
        {
            var formattedStringInput = new FormattedStringInput();
            var input = "//[***]\n11***22***33";

            //Check GetNumbersEnumerableFromInput return 3 elements enumarable 
            Assert.Equal(3, formattedStringInput.GetNumbersEnumerableFromInput(input).Count());
        }

        [Fact]
        public void GetNumbersEnumerableFromInput_should_throw_exception_when_invalid_pattern()
        {
            var formattedStringInput = new FormattedStringInput();
            //This patther is not valid becuase ';;' should be inside brackets
            //In order to be valid should be "//[;;]\n2;;5"
            var input = "//;;\n2;;5";

            Assert.Throws<InvalidInputDelimiterPatternException>(() => formattedStringInput.GetNumbersEnumerableFromInput(input));
        }

        [Fact]
        public void GetNumbersEnumerableFromInput_should_split_input_using_any_multiple_custom_delimiters()
        {
            var formattedStringInput = new FormattedStringInput();
            var input = "//[*][!!][rrr]\n11rrr22*33!!44";

            //Check GetNumbersEnumerableFromInput return 3 elements enumarable 
            Assert.Equal(4, formattedStringInput.GetNumbersEnumerableFromInput(input).Count());
        }

        [Theory]
        [InlineData("//[*][!!][\n11rrr22*33!!44")]
        [InlineData("//][**]\n11rrr22**33")]
        public void GetNumbersEnumerableFromInput_should_throw_exception_when_invalid_multiple_pattern(string input)
        {
            var formattedStringInput = new FormattedStringInput();

            Assert.Throws<InvalidInputDelimiterPatternException>(() => formattedStringInput.GetNumbersEnumerableFromInput(input));
        }
    }
}
