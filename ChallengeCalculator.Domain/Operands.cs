﻿using ChallengeCalculator.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChallengeCalculator.Domain
{
    public class Operands
    {
        readonly List<decimal> _operands;
        private Operands()
        {
            _operands = new List<decimal>();
        }

        static public Operands CreateOperandsFromNumbersEnumerable(IEnumerable<string> numbers)
        {
            var operands = new Operands();

            if (IsNumbersEnumerableNullOrHasNoElements(numbers))
                operands.AddOperand(0);

            var negativeNumbersList = new List<decimal>();
            foreach (var n in numbers)
            {
                if (decimal.TryParse(n, out var operand))
                {
                    try
                    {
                        operands.AddOperand(operand);
                    }
                    catch (NegativeNumberException e)
                    {
                        negativeNumbersList.Add(e.Number);
                    }
                }
                else
                    operands.AddOperand(0);
            }

            if (HasNegativeNumbers(negativeNumbersList))
                throw new AllNegativeNumbersException(negativeNumbersList);

            return operands;
        }

        public bool HasEny() => _operands.Count> 0;

        public IEnumerable<decimal> GetAllOperands() => _operands;

        public decimal GetFirst() => _operands.First();

        public IEnumerable<decimal> GetAllButFirst() => _operands.Skip(1);


        private void AddOperand(decimal operand)
        {
            if (operand < 0)
                throw new NegativeNumberException(operand);
            if (operand > 1000)
                _operands.Add(0);
            else
                _operands.Add(operand);
        }

        static private bool IsNumbersEnumerableNullOrHasNoElements(IEnumerable<string> numbersEnumerable)
        {
            if (numbersEnumerable == null || numbersEnumerable.Count() == 0)
                return true;
            return false;
        }

        static private bool HasNegativeNumbers(List<decimal> negativeNumbers)
        {
            return negativeNumbers.Count > 0;
        }
    }
}
