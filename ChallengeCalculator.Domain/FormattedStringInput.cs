﻿using ChallengeCalculator.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ChallengeCalculator.Domain
{
    public class FormattedStringInput
    {
        private const string DELIMITER_PATTERN = "^//.+\n";
        private const string ANY_LENGTH_OR_MULTIPLE_DELIMITER_BODY_PATTERN = "^((\\[.+\\])+)$";

        public IEnumerable<string> GetNumbersEnumerableFromInput(string input)
        {
            if (HasCustomDelimiters(input))
            {
                var delimiters = GetCustomDelimiters(input);
                var inputWithoutDelimiterPattern = DiscardDelimiterPatternFromInput(input);

                return inputWithoutDelimiterPattern.Split(delimiters, StringSplitOptions.None);
            }

            //If input does not contains a custom delimiter use either ',' or '\n' characters
            //as delimiters
            return input.Split(new[] { ',', '\n' }, StringSplitOptions.None);
        }

        private bool HasCustomDelimiters(string input) => Regex.IsMatch(input, DELIMITER_PATTERN);

        private string[] GetCustomDelimiters(string input)
        {
            var newlineIndex = input.IndexOf('\n');
            //Find what is between // and \n at the start and end of the pattern
            var delimiterBody = input.Substring(2, newlineIndex - 2);

            if (IsSingleCustomDelimiter(delimiterBody))
                return new[] { delimiterBody };

            if (IsValidAnyLengthOrMultipleDelimiter(delimiterBody))
                return DiscardBracketsFromDelimiterBody(delimiterBody);

            throw new InvalidInputDelimiterPatternException();
        }

        private bool IsValidAnyLengthOrMultipleDelimiter(string delimiterBody) => Regex.IsMatch(delimiterBody, ANY_LENGTH_OR_MULTIPLE_DELIMITER_BODY_PATTERN);

        private string DiscardDelimiterPatternFromInput(string input)
            => Regex.Replace(input, DELIMITER_PATTERN, string.Empty);

        private string[] DiscardBracketsFromDelimiterBody(string delimiterBody)
            => delimiterBody.Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);

        private bool IsSingleCustomDelimiter(string delimiterBody) => delimiterBody.Length == 1;
    }
}
