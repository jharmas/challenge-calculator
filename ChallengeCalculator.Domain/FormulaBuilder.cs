﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeCalculator.Domain
{
    public class FormulaBuilder
    {
        StringBuilder _stringBuilder;

        private bool IsFirstOperand { get; set; } = true;

        public FormulaBuilder()
        {
            _stringBuilder = new StringBuilder();           
        }
        public void AddOperand(decimal operand, Operator @operator)
        {
            if (IsFirstOperand)
                IsFirstOperand = false;
            else
            {
                switch (@operator)
                {
                    case Operator.Addition:
                        _stringBuilder.Append('+');
                        break;
                    case Operator.Substraction:
                        _stringBuilder.Append('-');
                        break;
                    case Operator.Multiplication:
                        _stringBuilder.Append('*');
                        break;
                    case Operator.Division:
                        _stringBuilder.Append('/');
                        break;
                }
            }

            _stringBuilder.Append(operand);
        }

        public void SetResult(decimal result)
        {
            if (IsFirstOperand)
                _stringBuilder.Append(result);

            _stringBuilder.Append(" = ");
            _stringBuilder.Append(result);
        }


        public string GetFormula()
        {
            if (IsFirstOperand)
                return "No formula used";
            return _stringBuilder.ToString();
        }
    }
}
