﻿using System;

namespace ChallengeCalculator.Domain
{
    public enum Operator
    {
        Addition,
        Substraction,
        Multiplication,
        Division
    }
    public class Calculator
    {
        private FormulaBuilder _formulaBuilder;

        public Calculator(){}

        public Calculator(FormulaBuilder formulaBuilder)
        {
            _formulaBuilder = formulaBuilder;
        }

        public decimal ExecuteOperation(Operands operands, Operator @operator)
        {
            if (operands.HasEny() == false)
                return 0;

            var result = operands.GetFirst();
            _formulaBuilder?.AddOperand(result, @operator);

            foreach (var o in operands.GetAllButFirst())
            {
                _formulaBuilder?.AddOperand(o, @operator);
                switch (@operator)
                {
                    case Operator.Addition:
                        result += o;
                        break;
                    case Operator.Substraction:
                        result -= o;
                        break;
                    case Operator.Multiplication:
                        result *= o;
                        break;
                    case Operator.Division:
                        result /= o;
                        break;
                }
            }

            _formulaBuilder?.SetResult(result);
            return result;
        }


    }
}
