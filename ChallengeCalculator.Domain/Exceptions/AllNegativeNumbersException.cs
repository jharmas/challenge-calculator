﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeCalculator.Domain.Exceptions
{
    public class AllNegativeNumbersException: Exception
    {
        public IEnumerable<decimal> NegativeNumbers { get; private set; }
        public AllNegativeNumbersException(IEnumerable<decimal> negativeNumbers)
        {
            NegativeNumbers = negativeNumbers;
        }
    }
}
