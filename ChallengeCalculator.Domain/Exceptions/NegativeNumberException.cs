﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeCalculator.Domain.Exceptions
{
    public class NegativeNumberException: Exception
    {
        public decimal Number { get; private set; }
        public NegativeNumberException(decimal number)
        {
            Number = number;
        }
    }
}
